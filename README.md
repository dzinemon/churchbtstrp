# Non-profit organization website
 __aidCIM.org__

***

## Latest build: [Gitlab pages](https://dzinemon.gitlab.io/churchbtstrp) 

## Forked project with custom domain: [Aid Christian International Ministry](http://www.aidcim.org/) 

***

a template made using 
[Bootstrap](http://getbootstrap.com/)
and 
[Jekyll](https://jekyllrb.com/)

***

images from 
[Pexels](https://www.pexels.com/) 
& 
[Placeholdit](https://placehold.it/)

icons
[Fontawesome](http://fontawesome.io/)

***

## Instructions

### Creating a regular page (Eng version):

1.1 Create a new-page.html in root folder of the project (same folder as index.html in)

2.1 Add front matter:

* layout: default
* description: Meta description for this page
* title: title for this page
* heading: Actually a name of the page visible to visitor

3.1 Add include for nav just after the frontmatter

{% include nav-desktop.html %}

4.1 And Place your content using html tags

### Creating a regular page (Rus version):

1.2 Create a new-page.html in "/ru" folder of the project

	*Important* - page name must be the same for eng and russian folders

2.2 Add front matter:

* layout: default-ru
* description: Meta description for this page
* title: title for this page
* heading: Actually a name of the page visible to visitor

3.2 Add include for nav just after the frontmatter

{% include nav-desktop-ru.html %}

4.2 And Place your content using html tags for russian version

***

### Creating a new current project ENG:

2.1 Copy existing project or create a new file another-project.html in _current-projects folder

2.2 Copy front matter or create new one. Example:

* layout: project
* category: current
* description: Meta description for this page
* title: title for this page
* img: /img/projects/16-1024x576.jpg - path to image
* project-title: "Project name"
* project-location: County name
* project-intro:

2.3 And Place your ENG content using html tags

### Creating a new current project RUS:

2.1 Copy existing project or create a new file another-project.html in _current-projects-ru folder

2.2 Copy front matter or create new one. Example:

* layout: project-ru
* category: current
* description: Meta description for this page
* title: title for this page
* img: /img/projects/16-1024x576.jpg - path to image
* project-title: "Project name"
* project-location: County name
* project-intro:

2.3 And Place your content using html tags
